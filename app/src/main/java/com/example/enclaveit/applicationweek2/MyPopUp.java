package com.example.enclaveit.applicationweek2;

import android.content.Context;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

/**
 * Created by enclaveit on 20/12/2016.
 */

public class MyPopUp {
    public MyPopUp(){};
    private PopupWindow popUp;
    private RelativeLayout popUpLayout;

    public void showPopUp(final Context context){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_layout, null);
            popUpLayout = (RelativeLayout) view.findViewById(R.id.popup_layout);
        /*public PopupWindow (View contentView, int width, int height)*/
        popUp = new PopupWindow(view, RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        /*Set an elevation value of PopUp. Required API >= 21*/
        if(Build.VERSION.SDK_INT >= 21){
            popUp.setElevation(5.0f);
        }

        ImageButton imgClose = (ImageButton) view.findViewById(R.id.imgBtnClosePopUp);
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popUp.dismiss();
                    Toaster.makeToast(context, "PopUp was dismissed", 2000);
                }
            });
        popUp.showAtLocation(popUpLayout, Gravity.CENTER,0,0);
    }
}
