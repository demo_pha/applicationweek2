package com.example.enclaveit.applicationweek2;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by enclaveit on 20/12/2016.
 */

public class CustomAdapter extends BaseAdapter {
    Context context;
    String[] listSearch;
    int[] listIcon;
    LayoutInflater inflater = null;
    public CustomAdapter(MainActivity mainActivity, String[] listSearch, int[] listIcon){
        this.context = mainActivity;
        this.listIcon = listIcon;
        this.listSearch = listSearch;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listSearch.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public class Share{
        TextView txtShare;
        ImageView icon;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        Share share = new Share();
            View rowShare = inflater.inflate(R.layout.list_share_layout, null);
            share.txtShare = (TextView) rowShare.findViewById(R.id.txtShare);
            share.icon = (ImageView) rowShare.findViewById(R.id.icon1);

            share.txtShare.setText(listSearch[i]);
            share.icon.setImageResource(listIcon[i]);
            rowShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toaster.makeToast(context, "You clicked the item share "+listSearch[i],3000);
                }
            });
        return rowShare;
    }
}
