package com.example.enclaveit.applicationweek2;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.enclaveit.applicationweek2.Service.PlayingMusicService;

import java.util.ArrayList;
import java.util.Random;

/**currentIndex
 * Created by PHA on 20/12/2016.
 */
/*Link Preference
*https://code.tutsplus.com/tutorials/create-a-music-player-on-android-user-controls--mobile-22787
* https://code.tutsplus.com/tutorials/create-a-music-player-on-android-song-playback--mobile-22778*
* https://www.tutorialspoint.com/android/android_mediaplayer.htm*
* https://developer.android.com/guide/topics/media/mediaplayer.html*/
public class PlayingMusicActionFragment extends Fragment implements View.OnClickListener{
    static ImageButton btnPrevious, btnNext, btnPlay, btnPause, btnShuffle, btnRepeat;
    static TextView txtTime, txtTotalTime, txtSongTitle;
    static SeekBar seekBarPlaying;
    static int currentIndex;
    Handler handler = new Handler();
    int duration;
    int currMin, currSec;
    int min, sec, currentTime;
    static int count = 0;
    boolean isPlaying = false;
    ArrayList<Song> listSong = new ArrayList<>();

    private Intent intentPlayingService;
    public PlayingMusicService playingMusicService;
    private boolean isShuffle= false, isRepeat = false;
    private Random random;

    BroadcastReceiver broadcastReceiver;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.play_music_action_fragment, viewGroup, false);
        btnPlay = (ImageButton) view.findViewById(R.id.btnPlay);
            btnPlay.setOnClickListener(this);
        btnPause = (ImageButton) view.findViewById(R.id.btnPause);
            btnPause.setOnClickListener(this);
        btnPrevious = (ImageButton) view.findViewById(R.id.btnPrevious);
            btnPrevious.setOnClickListener(this);
        btnNext = (ImageButton) view.findViewById(R.id.btnNext);
            btnNext.setOnClickListener(this);
        btnShuffle= (ImageButton) view.findViewById(R.id.btnShuffle);
            btnShuffle.setOnClickListener(this);
        btnRepeat = (ImageButton) view.findViewById(R.id.btnRepeat);
            btnRepeat.setOnClickListener(this);

        txtTime = (TextView) view.findViewById(R.id.txtTime);
        txtTotalTime = (TextView) view.findViewById(R.id.txtTotalTime);
        txtSongTitle = (TextView) view.findViewById(R.id.txtSongTitle);
        currentTime = 0;
        currentIndex = 0;

        seekBarPlaying = (SeekBar) view.findViewById(R.id.seekBarPlaying);
            seekBarPlaying.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int position, boolean b) {
                    //Log.i("Message", "Seekbar Position = "+position);
                   // seekBar.setProgress(position);
                }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    //Toaster.makeToast(getContext(), "Move to "+seekBar.getProgress(), 1000);
                }
                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    //Toaster.makeToast(getContext(), "Stop Tracking Touch", 1000);
                    Log.i("Message", "Change Seek bar "+seekBar.getProgress());
                    count = seekBar.getProgress();
                    seek(count*1000);
                }
            });

        /*Check permission*/
        int permission = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        if( permission != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
        Log.i("Message","Playing Music Action Fragment On Create View");
        return view;
    }


    @Override
    public void onStart(){
        super.onStart();
        /*Create Service*/
        if (intentPlayingService == null) {
            Log.i("Message", "Create new Intent");
            intentPlayingService = new Intent(getActivity().getBaseContext(), PlayingMusicService.class);
            getActivity().startService(intentPlayingService);
            getActivity().bindService(intentPlayingService, serviceConnection, Context.BIND_AUTO_CREATE);
        }/*End Create Service*/
    }

    /*Connect to Service*/
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            PlayingMusicService.MusicBinder musicBinder = (PlayingMusicService.MusicBinder) iBinder;
            //get Service
            playingMusicService = musicBinder.getService();
            setListSong(ListMusicFragment.listSong);
            //pass list Song
            playingMusicService.setListSong(listSong);
            //playSongService(currentIndex);
            Log.i("Message", "On Service Connected");
            Log.i("Message", "List Song size = "+listSong.size());
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.i("Message", "On Service Disconnected");
        }
    };


    /*Using for get Broadcast Receiver from Service*/
    @Override
    public void onResume(){
        super.onResume();
        IntentFilter intentFilter = new IntentFilter("com.example.appplicationweek2.CUSTOM_INTENT");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String result = intent.getStringExtra("result");
                Log.i("Message", "Result from Service = "+result);
                //TODO Play next if result == "complete"
                if(result.equalsIgnoreCase("complete")){
                    if (isRepeat) play(listSong.get(currentIndex), currentIndex);
                    else playNext();
                }
            }
        };
            /*Registering our receiver*/
            getActivity().registerReceiver(broadcastReceiver, intentFilter);
            Log.i("Message","Registering receiver");
    }

    @Override
    public void onPause(){
        super.onPause();
        /*unRegistering receiver*/
        getActivity().unregisterReceiver(broadcastReceiver);
        Log.i("Message","Unregister receiver");

    }

    public void play(Song song, int index){
        isPlaying = true;       count = 0;
        if(index >= 0) currentIndex = index;
        Log.i("Message", "Current index = "+currentIndex);
        playSongService(currentIndex);

        duration = (int) song.getDuration()/1000;
        seekBarPlaying.setProgress(0);
        seekBarPlaying.setMax(duration);
        min = duration / 60;
        sec = duration - (min * 60);
        txtSongTitle.setText(song.getTitle());
        txtTotalTime.setText(((min < 10) ? "0" + min : min) + ":" + ((sec < 10) ? "0" + sec : sec) );
        btnPlay.setVisibility(View.GONE);
        btnPause.setVisibility(View.VISIBLE);
        Log.i("Message","Prepared -- Duration: "+duration);
        setRunnableCountTime(duration);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnPlay:
                btnPlay.setVisibility(View.GONE);
                btnPause.setVisibility(View.VISIBLE);
                resumePlayer();
                //TODO Resume music
                break;
            case R.id.btnPause:
                btnPause.setVisibility(View.GONE);
                btnPlay.setVisibility(View.VISIBLE);
                pausePlayer();
                //TODO Pause music
                break;
            case R.id.btnPrevious:
                playPrevious();
                Toaster.makeToast(getContext(), "Play Previous Song", 1000);
                break;
            case R.id.btnNext:
                Toaster.makeToast(getContext(), "Play Next Song", 1000);
                playNext();
                break;
            case R.id.btnShuffle:
                if(isShuffle == false){
                    isShuffle = true;
                    btnShuffle.setImageResource(R.drawable.shuffle);
                    Toaster.makeToast(getContext(), "Shuffle", 1000);
                }else {
                    isShuffle = false;
                    btnShuffle.setImageResource(R.drawable.no_shuffle);
                    Toaster.makeToast(getContext(), "No Shuffle", 1000);
                }
                break;
            case R.id.btnRepeat:
                if(!isRepeat){
                    isRepeat = true;
                    btnRepeat.setImageResource(R.drawable.repeat);
                    Toaster.makeToast(getContext(), "Repeat One", 1000);
                }else {
                    isRepeat = false;
                    btnRepeat.setImageResource(R.drawable.no_repeat);
                    Toaster.makeToast(getContext(), "Repeat All", 1000);
                }
                break;
        }
    }

    public void stop(){
        //player.release();
        Log.i("Message","Stopped playing");
    }
    public void pausePlayer(){
        playingMusicService.pauseSong();
        isPlaying = false;
    }
    public void resumePlayer(){
        playingMusicService.resumeSong();
        isPlaying = true;
    }

    public void playSongService(int position){
        playingMusicService.setSong(position);
        playingMusicService.playSong();
    }

    public void seek(int position){
        if (isPlaying) playingMusicService.seekTo(position);
    }

    public void playNext(){
        /*Stop the Runnable Count Time*/
        handler.removeCallbacks(UpdateSongTime);
        /*Check if set Shuffle*/
        if (isShuffle){
            random = new Random();
            currentIndex = random.nextInt(listSong.size()-1);
            play(listSong.get(currentIndex), currentIndex);
        }else{
            /*Check if current song is last*/
            if(currentIndex >= (listSong.size()-1)){
                currentIndex = 0;
                play(listSong.get(0) , currentIndex);
            }else{
                ++currentIndex;
                Log.i("Message", "Current Index : "+currentIndex);
                play(listSong.get(currentIndex), currentIndex);
            }
        }
    }

    public void playPrevious(){
        /*Stop the Runnable Count Time*/
        handler.removeCallbacks(UpdateSongTime);
        /*Check if set Shuffle*/
        if (isShuffle){
            random = new Random();
            currentIndex = random.nextInt(listSong.size()-1);
            play(listSong.get(currentIndex), currentIndex);
        }else {
            /*Check if current song is first*/
            if (currentIndex == 0) {
                currentIndex = listSong.size() - 1;
                play(listSong.get(currentIndex), currentIndex);
            } else {
                --currentIndex;
                Log.i("Message", "Current Index : " + currentIndex);
                play(listSong.get(currentIndex), currentIndex);
            }
        }
    }
/*Check permission using Read-Write External Store*/
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                }
                break;

            default:
                break;
        }
    }

    private Runnable UpdateSongTime;
    private void setRunnableCountTime(final int duration) {
        UpdateSongTime = new Runnable() {
            @Override
            public void run() {
                if (isPlaying && count <= duration) {
                    currentTime = count++;
                    currMin = currentTime / 60;
                    currSec = currentTime % 60;
                    seekBarPlaying.setProgress(currentTime);
                    txtTime.setText(((currMin < 10) ? "0" + currMin : currMin) + ":" + ((currSec < 10) ? "0" + currSec : currSec));
                }
                handler.postDelayed(this, 1000);
            }
        };
        UpdateSongTime.run();
    }

    public void setListSong(ArrayList<Song> listSong) {
        this.listSong = listSong;
    }
}