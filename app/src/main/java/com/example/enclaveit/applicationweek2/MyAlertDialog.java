package com.example.enclaveit.applicationweek2;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.widget.Toast;

/**
 * Created by enclaveit on 19/12/2016.
 */

public class MyAlertDialog {

    public static void alertDialog(final Context context){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setTitle("This is an alert Dialog");
            alert.setIcon(R.mipmap.alert);
            alert.setPositiveButton("Show Toast", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //Toast.makeText(context, "This is a Toast message",Toast.LENGTH_SHORT).show();
                    Toaster.makeToast(context, "This is a Toast message for changed time", 8000);
                }
            }).setNegativeButton("Show PopUp", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                    MyPopUp myPopUp = new MyPopUp();
                        myPopUp.showPopUp(context);
                }
            });

        AlertDialog dialog = alert.create();
            dialog.show();
    }
}
