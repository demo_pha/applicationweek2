package com.example.enclaveit.applicationweek2;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by PHA on 20/12/2016.
 */

public class PlayMusicActivity extends FragmentActivity {
public static ViewPager pager;
    public static  PlayingMusicActionFragment playingMusicActionFragment;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_music_layout);
         pager = (ViewPager) findViewById(R.id.pager);
        FragmentManager fragmentManager = getSupportFragmentManager();
        PagerAdapter pagerAdapter = new PagerAdapter(fragmentManager);
        playingMusicActionFragment = new PlayingMusicActionFragment();
        fragmentManager.beginTransaction().replace(R.id.play_music_action_fragment, playingMusicActionFragment).commit();
        pager.setAdapter(pagerAdapter);
        pager.setCurrentItem(0);

    }
}
