package com.example.enclaveit.applicationweek2;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TableLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    /*Button for Media play music*/
    static Button btnMediaPlayer;
    MyMediaPlayer myMediaPlayer;

    /*Button for Change Background*/
    private static final int REQUEST_CODE = 1;
    Button btnChangeBackground;
    public static TableLayout main ;

    /*Button for Alert Dialog and Toast*/
    Button btnAlertDialogAndToast;

    /*For SnackBar*/
    CoordinatorLayout coordinatorLayout;
    boolean isUndo = false;
    String fullPath = "";
    Snackbar snackbar;
    DBHelper dbHelper;

  /*  *//*For List Search*//*
    final String[] listSearchString = {"Facebook","Skype","Twitter","Bluetooth"};
    final int[] listIconSearch = {R.mipmap.facebook, R.mipmap.skype, R.mipmap.twitter, R.mipmap.bluetooth};*/

    /*For Playing Music*/
    Button btnPlayingMusic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("Message","Main On Create");

        /*Using ToolBar*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

        /*Using ListView*//*
        ListView listShare = (ListView) findViewById(R.id.listShare);
        listShare.setAdapter(new CustomListSongAdapter(MainActivity.this, listSearchString, listIconSearch));*/


        /*Media Player playing music*/
        btnMediaPlayer = (Button) findViewById(R.id.btnPlayMedia);
            btnMediaPlayer.setOnClickListener(this);

        /*For Playing Music*/
        btnPlayingMusic = (Button) findViewById(R.id.btnPlayingMusic);
        btnPlayingMusic.setOnClickListener(this);

        /*Change Background*/
        main = (TableLayout) findViewById(R.id.activity_main);
        btnChangeBackground = (Button) findViewById(R.id.btnChangeBackground);
            btnChangeBackground.setOnClickListener(this);
        dbHelper = new DBHelper(MainActivity.this);
                /*Change background if exists in DB*/
                    if(dbHelper.getImagePath()!=null) {
                        File ex_image = new File(dbHelper.getImagePath());
                        if (ex_image.exists()) {
                            Log.i("Message","Init background");
                            Bitmap bitmap = BitmapFactory.decodeFile(ex_image.getAbsolutePath());
                            main.setBackgroundDrawable(new BitmapDrawable(getResources(), bitmap));
                        }
                    }

        /*Show Alert Dialog and Toast*/
        btnAlertDialogAndToast = (Button) findViewById(R.id.btnAlertDialogAndToast);
            btnAlertDialogAndToast.setOnClickListener(this);

        /*For SnackBar*/
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
    }


    @Override
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btnPlayMedia:
                myMediaPlayer= new MyMediaPlayer();
                myMediaPlayer.playMusic(this.getApplicationContext());
                btnMediaPlayer.setClickable(false);
                break;
            case R.id.btnPlayingMusic:
                Intent intentPlayingMusic = new Intent(MainActivity.this, PlayMusicActivity.class);
                startActivity(intentPlayingMusic);
                break;
            case R.id.btnChangeBackground:
                isUndo = false;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent,"Select Picture"), REQUEST_CODE );
                break;
            case R.id.btnAlertDialogAndToast:
                MyAlertDialog.alertDialog(MainActivity.this);
                break;
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        Log.i("Message","Main On Paused");
        if (myMediaPlayer != null )myMediaPlayer.stop();
        if(snackbar != null ) snackbar.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturn){
        super.onActivityResult(requestCode,resultCode,imageReturn);
        switch (requestCode){
            case REQUEST_CODE:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturn.getData();
                    fullPath= getFullPath(selectedImage);
                    Log.i("New Image Path",fullPath);
                    try {
                        //TODO Save image
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                        Drawable drawable = new BitmapDrawable(getResources(), bitmap);
                        main.setBackgroundDrawable(drawable);
                        /*TODO Using SnackBar to undo Background*/
                        snackbar = Snackbar.make(coordinatorLayout, "Background has changed",Snackbar.LENGTH_LONG)
                                            .setDuration(5000)
                                .setCallback(new Snackbar.Callback() {
                                    @Override
                                    public void onDismissed(Snackbar snackbar, int event) {
                                        super.onDismissed(snackbar, event);
                                        switch (event){
                                            case Snackbar.Callback.DISMISS_EVENT_MANUAL:
                                            case Snackbar.Callback.DISMISS_EVENT_TIMEOUT:
                                                Log.i("Message","Snackbar is not shown");
                                                main.setTag(fullPath);
                                                Log.i("TAG", main.getTag().toString());
                                                dbHelper.saveBackground(fullPath);
                                                break;
                                        }
                                    }
                                })
                                .setAction("Undo", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        //return ex_background
                                        isUndo = true;
                                        Log.i("Message", "Ex_Image: "+main.getTag());
                                        File ex_image = new File(main.getTag().toString());
                                        if(ex_image.exists()){
                                        /*TODO get image from filename*/
                                            Bitmap bitmap1 = BitmapFactory.decodeFile(ex_image.getAbsolutePath());
                                            main.setBackgroundDrawable(new BitmapDrawable(getResources(), bitmap1));
                                            Toast.makeText(getApplication().getApplicationContext(), "Back to Previous Background succes", Toast.LENGTH_SHORT).show();
                                        }else{
                                            Log.i("Message", "File not found! Change background to default.");
                                            main.setBackgroundDrawable(getResources().getDrawable(R.drawable.anime_girl_wapper));
                                        }
                                        Log.i("Message", "Returning background");
                                    }
                                });
                        snackbar.setActionTextColor(Color.RED);
                            snackbar.show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        }
    }

    public String getFullPath(Uri uri){
        String realPath;
        // SDK < API11
        if (Build.VERSION.SDK_INT < 11)
            realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, uri);

            // SDK >= 11 && SDK < 19
        else if (Build.VERSION.SDK_INT < 19)
            realPath = RealPathUtil.getRealPathFromURI_API11to18(this, uri);

            // SDK > 19 (Android 4.4)
        else
            realPath = RealPathUtil.getRealPathFromURI_API19(this, uri);
        return realPath;
    }

    /*Using ToolBar*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        /*Inflate Menu; This adds items to menu if it is presented*/
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /*Event when click items of Menu ToolBar*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.menu_profile:
                Toaster.makeToast(MainActivity.this, "You clicked Profile Item of ToolBar", 5000);
                return true;
            case R.id.menu_mailBox:
                Toaster.makeToast(MainActivity.this, "You clicked MailBox Item of ToolBar", 5000);
                return true;
            case R.id.shareFacebook:
                Toaster.makeToast(MainActivity.this, "You clicked Share Facebook Item of ToolBar", 5000);
                return true;
            case R.id.shareSkype:
                Toaster.makeToast(MainActivity.this, "You clicked Share Skype Item of ToolBar", 5000);
                return true;
            case R.id.shareTwitter:
                Toaster.makeToast(MainActivity.this, "You clicked Share Twitter Item of ToolBar", 5000);
                return true;
            case R.id.shareBluetooth:
                Toaster.makeToast(MainActivity.this, "You clicked Share BlueTooth Item of ToolBar", 5000);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
