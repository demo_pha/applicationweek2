package com.example.enclaveit.applicationweek2;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by enclaveit on 20/12/2016.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "week2";
    private static final String TABLE_NAME = "background";
    private static final int DB_VERSION = 1;
    private static final String KEY_IMAGE = "image";

    private static final String DB_CREATE_BACKGROUND = "create table "+TABLE_NAME+" ("+KEY_IMAGE+" text not null)";
    private static final String DB_BACKGROUND_INIT = "insert into background(\""+KEY_IMAGE+"\") values(\"\")";

    public DBHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("MESSAGE","Create an init Background TABLE");
        db.execSQL(DB_CREATE_BACKGROUND);
        db.execSQL(DB_BACKGROUND_INIT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("Drop table if exists "+TABLE_NAME);
        onCreate(db);
    }

    /*Change image background*/
    /*Save the image Path*/
    public void saveBackground(String path){
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "update "+TABLE_NAME+" set image = \""+path+"\" where image IS NOT NULL";
        db.execSQL(sql);
        Log.i("Message","Save Image Path to DB");
    }

    public String getImagePath(){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "Select image from "+TABLE_NAME;
        Cursor cursor = db.rawQuery(sql,null);
        if (!cursor.moveToNext()) return null;
        return cursor.getString(0);
    }
}
