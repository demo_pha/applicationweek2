package com.example.enclaveit.applicationweek2.Service;

import android.app.Service;
import android.content.ContentUris;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;

import com.example.enclaveit.applicationweek2.Song;
import com.example.enclaveit.applicationweek2.Toaster;

import java.io.IOException;
import java.util.ArrayList;

public class PlayingMusicService extends Service implements MediaPlayer.OnPreparedListener{
    private MediaPlayer player;
    public ArrayList<Song> listSong;
    private int currentSongIndex;
    private final IBinder musicBind = new MusicBinder();

    public PlayingMusicService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i("Message", "On Bind Service ");
        // TODO: Return the communication channel to the service.
        //throw new UnsupportedOperationException("Not yet implemented");
        return musicBind;
    }

    @Override
    public boolean onUnbind(Intent intent){
        player.stop();
        player.release();
        Log.i("Message", "On UnBind Service");
        return false;
    }

    @Override
    public void onCreate(){
        super.onCreate();
        currentSongIndex = 0;
        player = new MediaPlayer();
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        Log.i("Message", "On Create Service - Current Index = "+currentSongIndex);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        //playSong();
        Log.i("Message", "On Start Command Service ");
        return START_STICKY;
    }


    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        player.start();
        Log.i("Message", "On Prepared - Current Index = "+currentSongIndex);
    }

    public void playSong(){
        player.reset();
//        if (!player.isPlaying()) Log.i("Message", "Player is null");
        Song song = listSong.get(currentSongIndex);
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                //mediaPlayer.release();
                Log.i("Message", "Complete");
                Intent sendResult = new Intent("com.example.appplicationweek2.CUSTOM_INTENT");
                    sendResult.putExtra("result","complete");
                sendBroadcast(sendResult);
            }
        });
        try {
            Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Long.parseLong(song.getId()));
            player.setDataSource(this, uri );
            player.setOnPreparedListener(this);
            //prepare for buffering, ...
            player.prepareAsync();
            player.setVolume(100f, 100f);
            Log.i("Message", "Play Music -"+ song.getTitle());
            //txtSongTitle.setText(song.getTitle());
        } catch (IOException e) {
            Log.d("Error", "Error when playing music ");
            e.printStackTrace();
        }
    }

    public void pauseSong(){
        if (player.isPlaying()){
            player.pause();
            Log.i("Message", "Pause Song");
            Toaster.makeToast(this, "Paused",1000);
        }
    }

    public void resumeSong(){
        if(player != null) {
            player.start();
            Log.i("Message", "Re-play music");
            Toaster.makeToast(this,"Playing", 1000);
        }
    }

    public void setSong(int index){
        currentSongIndex = index;
    }

    public void setListSong(ArrayList<Song> listSongs){
        this.listSong = listSongs;
        Log.i("Message","List Song service = "+listSong.size());
    }

    public void seekTo(int position) {
        Log.i("Message", "Player position = "+position);
        player.seekTo(position);
    }

    public class MusicBinder extends Binder{
        public PlayingMusicService getService(){
            return PlayingMusicService.this;
        }
    }

    @Override
    public void onDestroy(){
        player.release();
        super.onDestroy();
        Log.i("Message"," On Destroy Service");
    }
}
