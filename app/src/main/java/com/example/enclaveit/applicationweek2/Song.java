package com.example.enclaveit.applicationweek2;

import java.io.Serializable;

/**
 * Created by enclaveit on 21/12/2016.
 */

public class Song implements Serializable {
    private String id, title, artist;
    private long duration;
    public Song(){};
    public Song(String id,String title, String artist, long duration){
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.duration = duration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }
}
