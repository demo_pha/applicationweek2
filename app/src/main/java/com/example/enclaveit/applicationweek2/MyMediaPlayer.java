package com.example.enclaveit.applicationweek2;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

/**
 * Created by enclaveit on 19/12/2016.
 */

public class MyMediaPlayer implements MediaPlayer.OnPreparedListener{
    MediaPlayer mediaPlayer;
    /*Play music from http://www.nhaccuatui.com/bai-hat/alone-alan-walker.dPAWTe6nAnZ8.html*/
    public void playMusic(Context context){
        String url = "http://www.hubharp.com/web_sound/BachGavotte.mp3";
        mediaPlayer= new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                Log.i("Message", "Complete Playing");
                mp.release();
                MainActivity.btnMediaPlayer.setClickable(true);
            }

        });
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.setOnPreparedListener(this);
            //prepare for buffering, ...
            mediaPlayer.prepareAsync();
            mediaPlayer.setVolume(100f, 100f);
            Toast.makeText(context, "Playing music", Toast.LENGTH_SHORT).show();
            Log.i("Message","Playing music");
            //mediaPlayer.start();
        } catch (IOException e) {
            Log.d("Error", "Error when playing music from http://www.hubharp.com/web_sound/BachGavotte.mp3");
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
        Log.i("Message","Prepared");
    }

    public void stop(){
        mediaPlayer.release();
        Log.i("Message","Stopped playing");
    }
}
