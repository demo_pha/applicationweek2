package com.example.enclaveit.applicationweek2;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.enclaveit.applicationweek2.Service.PlayingMusicService;

import java.util.ArrayList;


/**
 * Created by enclaveit on 21/12/2016.
 */

public class ListMusicFragment extends Fragment {
    public static ArrayList<Song> listSong;
    public PlayingMusicService playingMusicService;
    private boolean musicBound=false;
    PlayingMusicActionFragment playingMusicActionFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.list_music_fragment, viewGroup, false);
        ListView listMusic = (ListView) view.findViewById(R.id.listMusic);
        /*TODO Read all music from devices*/
        ContentResolver store = getActivity().getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection  = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        final String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION
        };
        Cursor cursor = store.query(uri, projection,selection, null, sortOrder);
        String allTitle = "";
        listSong = new ArrayList<>();
        if(cursor != null && cursor.moveToFirst()){
                do {
                     allTitle += cursor.getString(2)+" -- "+cursor.getString(0)+"    ";
                    Song song = new Song(cursor.getString(0), cursor.getString(2), cursor.getString(1), cursor.getLong(5));
                    listSong.add(song);
                }while (cursor.moveToNext());
            Log.i("Message","All Title: "+allTitle);
            /*playingMusicActionFragment = new PlayingMusicActionFragment();
            *//*Set List Song of Playing Music Action Fragment*//*
            playingMusicActionFragment.setListSong(listSong);*/

            /*Add to List Music*/
            listMusic.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Song song =  (Song) adapterView.getAdapter().getItem(i);
                    //Go to SubScript Music
                    PlayMusicActivity.pager.setCurrentItem(1);
                    //playSongService(i);
                    play(song, i);
                }
            });
            listMusic.setAdapter(new CustomListSongAdapter(view.getContext(), listSong));
        }else Toaster.makeToast(view.getContext(),"No Entry",3000);

        return view;
    }


    public void play(Song song, int index){
        //playingMusicActionFragment = (PlayingMusicActionFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.play_music_action_fragment);
        playingMusicActionFragment = PlayMusicActivity.playingMusicActionFragment;
        /*Bundle bundle = new Bundle();
        bundle.putInt("index",index);
        playingMusicActionFragment.setArguments(bundle);*/
        playingMusicActionFragment.play(song, index);
    }


}
