package com.example.enclaveit.applicationweek2;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.enclaveit.applicationweek2.Song;

import java.util.ArrayList;

/**
 * Created by enclaveit on 20/12/2016.
 */

public class CustomListSongAdapter extends BaseAdapter {
    Context context;
    String[] listTitle;
    int[] listId;
    long[] listDuration;
    String[] listArtist;
    ArrayList<Song> listSong;
    LayoutInflater inflater = null;
    public CustomListSongAdapter(Context playMusicActivity, ArrayList<Song> listSong){// String[]listTitle, int[] listId, String[] listArtist, long[] listDuration){
        this.context = playMusicActivity;
        this.listSong = listSong;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listSong.size();
    }

    @Override
    public Song getItem(int i) {

        return listSong.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        Song song   = listSong.get(i);
            View rowSong = inflater.inflate(R.layout.list_song_item_layout, null);
        TextView txtTitle = (TextView) rowSong.findViewById(R.id.txtTitle);
        TextView txtID = (TextView) rowSong.findViewById(R.id.txtID);
        TextView txtArtist = (TextView) rowSong.findViewById(R.id.txtArtist);
        TextView txtDuration = (TextView) rowSong.findViewById(R.id.txtDuration);
        long duration = song.getDuration();
        int min = (int) duration/(60*1000);
        int sec = (int) (duration/1000) - (min*60);
            txtTitle.setText(song.getTitle());
            txtID.setText(song.getId());
            txtArtist.setText(song.getArtist());
            txtDuration.setText(String.valueOf(((min<10)?"0"+min:min)) + ":"+((sec<10)?"0"+sec:sec));
        return rowSong;
    }
}
