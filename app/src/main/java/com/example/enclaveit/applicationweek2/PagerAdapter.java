package com.example.enclaveit.applicationweek2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by enclaveit on 21/12/2016.
 */

public class PagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 2;
    public PagerAdapter(FragmentManager fm ){
        super(fm);
    }


    @Override
    public Fragment getItem(int position){
        switch (position){
            case 0:
                return new ListMusicFragment();
            case 1:
                return new SubScriptFragment();
            default:return null;
        }
    }

    @Override
    public int getCount(){
        return PAGE_COUNT;
    }
}
